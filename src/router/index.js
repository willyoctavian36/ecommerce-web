import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AddCategory from '@/views/Category/AddCategory.vue'
import Category from '@/views/Category/ListCategory.vue'
import Admin from '@/views/AdminPage.vue'
import ListProduct from '@/views/Product/ListProduct.vue'
import AddProduct from '@/views/Product/AddProduct.vue'
import EditCategory from '@/views/Category/EditCategory.vue'
import EditProduct from '@/views/Product/EditProduct.vue'
import DetailProduct from '@/views/Product/DetailProduct.vue'
import ListProductByCategory from '@/views/Category/ListProductByCategory.vue'
import SignUp from '@/views/SignUp.vue'
import SignIn from '@/views/SignIn.vue'
import WishList from '@/views/Product/WishList.vue'
import ProductCart from '@/views/Product/ProductCart.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomeView
  },
  {
    path: '/category/show/:id',
    name: 'ListProductsByCategory',
    component: ListProductByCategory
  },
  {
    path: '/admin/category/add',
    name: 'AddCategory',
    component: AddCategory
  },
  {
    path: '/admin/category',
    name: 'Category',
    component: Category
  },
  {
    path: '/admin',
    name: 'Admin',
    component: Admin
  },
  {
    path: '/admin/product',
    name: 'Product',
    component: ListProduct
  },
  {
    path: '/admin/product/new',
    name: 'AddProduct',
    component: AddProduct
  },
  {
    path: '/admin/category/edit/:id',
    name: 'EditCategory',
    component: EditCategory
  },
  {
    path: '/admin/product/edit/:id',
    name: 'EditProduct',
    component: EditProduct
  },
  {
    path: '/product/detail/:id',
    name: 'DetailProduct',
    component: DetailProduct
  },
  {
    path: '/signup',
    name: 'Signup',
    component: SignUp
  },
  {
    path: '/signin',
    name: 'Signin',
    component: SignIn
  },
  {
    path: '/wishlist',
    name: 'WishList',
    component: WishList
  },
  {
    path: '/cart',
    name: 'Cart',
    component: ProductCart
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
